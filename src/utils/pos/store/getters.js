const getters = {

  itemInfo: state => {
    return  JSON.parse(state.item) || []
  },
  categoryInfo: state => {
    return  JSON.parse(state.cat) || []
  },
  // userUpdated: state => state.userUpdated,
  // userPasswordUpdated: state => state.userPasswordUpdated,
  // userPasswordUpdateError: state => state.userPasswordUpdateError,
  // userPasswordUpdateErrorMsg: state => state.userPasswordUpdateErrorMsg,
}

export default getters
