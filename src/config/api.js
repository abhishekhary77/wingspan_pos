export default {
  url: 'http://localhost:8080',
  path: {
    prefix: 'api/method',
    storage: 'storage',
    upload: 'files/file-upload',
  },
}
