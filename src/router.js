import Vue from 'vue'
import Router from 'vue-router'
import Crud from './routes/Crud.vue'
import App from './routes/App.vue'
import Login from './routes/Login.vue'
// import Logged from './routes/POS.vue'
import POS from './routes/POS.vue'
import Transactions from './routes/transactions.vue'
import Settings from './routes/settings.vue'
import Customers from './routes/customers.vue'
import Orders from './routes/orders.vue'
import Products from './routes/products.vue'
import Dashboard from './routes/dashboard.vue'
import store from '@/store/'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/crud',
      name: 'crud',
      component: Crud,
    },
    {
      path: '/app',
      name: 'app',
      component: App,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (auth) {
          next('/')
        } else {
          next()
        }
      },
    },
    {
      path: '/',
      name: 'pos',
      component: POS,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },

    {
      path: '/transactions',
      name: 'Transactions',
      component: Transactions,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },

    {
      path: '/customers',
      name: 'Customers',
      component: Customers,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/orders',
      name: 'Orders',
      component: Orders,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/products',
      name: 'Products',
      component: Products,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    }


  ],
})

export default router
